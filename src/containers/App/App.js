import React, { useState } from 'react';
import './App.css';
import Board from '../../components/Board/Board';
import TriesCounter from '../../components/TriesCounter/TriesCounter';
import ResetButton from '../../components/ResetButton/ResetButton';
import StatusPanel from '../../components/StatusPanel/StatusPanel';

const App = () => {
  const generateField = () => {
    const field = [];
    for (let i = 0; i < 36; i++) {
      field.push({ hasItem: false, id: "sl" + i, isOpened: false });
    }
    const itemSlot = Math.floor(Math.random() * 36);
    field[itemSlot].hasItem = true;
    console.log(field);
    return field
  };

  const [field, setField] = useState(generateField);

  const [gameStatus, setGameStatus] = useState({
    count: 0,
    status: "Item has not been discovered",
    itemFound: false
  });

  const newField = () => {
    let newF = generateField;
    setField(newF);
    const gameStatusCopy = gameStatus;
    gameStatusCopy.count = 0;
    gameStatusCopy.itemFound = false;
    gameStatusCopy.status = "Item has not been discovered";
    setGameStatus(gameStatusCopy);
  };

  const openSlot = id => {
    if (gameStatus.itemFound === true) {
      alert("Press RESET button to start over!")
    } else {
      const index = field.findIndex(slot => slot.id === id);
      const slotCopy = field[index];
      slotCopy.isOpened = true;
      const fieldCopy = [...field];
      fieldCopy[index] = slotCopy;
      setField(fieldCopy);
      const gameStatusCopy = gameStatus;
      gameStatusCopy.count++;
      if (slotCopy.hasItem === true) {
        const statusCopy = gameStatus;
        statusCopy.status = "You have found the Item in " + statusCopy.count + " tries!";
        statusCopy.itemFound = true;
        setGameStatus(statusCopy);
      };
      setGameStatus(gameStatusCopy);
    };
  };

  let board = <Board
    field={field}
    onClick={openSlot}
  />;

  return (
    <div className="App">
      <StatusPanel status={gameStatus.status}/>
      <div className="PlayingField">
        {board}
      </div>
      <TriesCounter count={gameStatus.count} />
      <ResetButton onClick={newField} />
    </div>
  );
};

export default App;
