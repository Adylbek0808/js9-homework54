import React from 'react';

const TriesCounter = props => {
    return (
        <div>
            <p>Tries:{props.count}</p>            
        </div>
    );
};

export default TriesCounter;