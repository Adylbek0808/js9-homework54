import React from 'react';
import './Slot.css';


const Slot = props => {
    let slotClasses = ['Slot'];
    let itemClasses = [];
    if (props.isOpened === true) {
        slotClasses.push('Open');

    };
    let hasItem;
    if (props.hasItem === true && props.isOpened === true) {
        hasItem = 'X';
        itemClasses = "HasItem";
    };
    return (
        <div className={slotClasses.join(" ")} onClick={props.onClick}><p className={itemClasses}>{hasItem}</p></div>
    );

}
export default Slot;