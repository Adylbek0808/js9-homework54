import React from 'react';

const ResetButton = props => {
    return (
        <div><button onClick={props.onClick}>RESET</button></div>
    );
};

export default ResetButton;