import React from 'react';
import Slot from '../Slot/Slot';


const Board = props => {    
    return props.field.map((piece) => {
        return <Slot
            key={piece.id}
            hasItem={piece.hasItem}
            isOpened={piece.isOpened}
            onClick={() => props.onClick(piece.id)}
        />
    });
    

};

export default Board;